/**
 * @file
 *
 * Piecentage takes an array from the Drupal settings array and replaces
 * all the fieldsets found with a pie chart based input widget. The individual
 * wedges are number fields (also taken from the Drupal settings array) and
 * the total of all the number fields will equal 1.
 *
 * Code Monkey: Gold <gold@catalyst.net.nz> http://plus.google.com/+CitizenGold
 * Prototype: Andrew Caudwell <andrewc@catalyst.net.nz>
 * Built using: RaphaelJS http://raphaeljs.com/
 */
(function ($, Drupal, window, document, undefined) {
  $(function () {
    forms = Drupal.settings.piecentage;
    for (form_id in forms) {
      fieldsets = forms[form_id];
      //The fieldset IDs are our pie_id values
      for (pie_id in fieldsets) {
        // Initilise each of our fieldsets.
        num_fields = forms[form_id][pie_id]['num_fields'];
        min_slice_val = forms[form_id][pie_id]['minimum_slice_val'];
        initPiecentage(pie_id, num_fields, min_slice_val);
      }
    }
  });
})(jQuery, Drupal, this, this.document);

/**
 * Initilise a fieldset to use our input widget
 */
function initPiecentage(pie_id, targets, minimum_slice_percentage) {
  var preset = new Array();
  var click_count = {};
  
  var stroke_width_pie_dish = 3;
  var stroke_width_slice = 1;
  var stroke_width_swatch_border = 1;

  // Determine the chunk of the screen we'll take up.
  var max_height = 200;
  var height = $('#' + pie_id).height();
  if (height != max_height) {
    // We don't need a width greater than this
    height = max_height;
    // We have the height. Now allow the fieldset to expand as it needs to.
    $('#' + pie_id).height('auto');
  }
  var width = $('#' + pie_id).width();
  
  // Numbers for the chart
  var dish_x = parseInt(width / 2 - 10);
  var dish_y = parseInt(height / 2 - 10);
  if (dish_x > dish_y) {
    dish_x = dish_y;
  } else {
    dish_y = dish_x;
  }
  var dish_r = dish_y - 10;

  var r = Raphael(pie_id, width, height);

  var buttons = r.set();
  var keys = r.set();
  var slices = r.set();
  var rankings = r.set();
  var pie_dish;

  // The slice the mouse is currently over.
  var slice_over = -1;

  // register for mousewheel event
  $('#' + pie_id).bind('mousewheel', function (event, delta) {

    if (slice_over >= 0) {
      if (delta > 0) {
        expandSlice(slice_over, 1.5);
      } else {
        shrinkSlice(slice_over, 1.5);
      }
      return false;
    }
    return true;
  });

  r.customAttributes.slice = function (i, x, y, r, a1, a2) {

    var flag = (a2 - a1) > 180,
      clr = (a2 - a1) / 360;

    var rad1 = (a1 % 360) * Math.PI / 180;
    var rad2 = (a2 % 360) * Math.PI / 180;

    return {
      path: [
        ["M", x, y],
        ["l", r * Math.cos(rad1), r * Math.sin(rad1)],
        ["A", r, r, 0, +flag, 1, x + r * Math.cos(rad2), y + r * Math.sin(rad2)],
        ["z"]
      ],
      gradient: ((a1 % 360)) + "-#eee-" + preset[i].colour
    };
  };

  function animateSlices(ms) {
    var start = 270,
      val;
    for (i = 0; i < preset.length; i++) {
      val = 360 / 100 * preset[i].weight;
      slices[i].animate({
        slice: [i, dish_x, dish_y, dish_r, start, start += val]
      }, ms || 1500, "linear");
    }
  }

  // NOTE: could scale input weights to add up to 100 ?

  // NOTE: could sort here if we wanted to

  function setTooltips() {
    for (var i = 0; i < slices.length; i++) {
      var percent = Math.floor(preset[i].weight * 10) / 10;
      slices[i].attr({
        title: preset[i].label + " " + (percent) + "%"
      });
    }
  }


  function normalizeData() {

    var sum_total = 0;

    for (i = 0; i < preset.length; i++) {
      sum_total += preset[i].weight;
    }

    var new_total = 0;
    var largest_weight = 0;
    var largest_i = 0;
    for (i = 0; i < preset.length; i++) {
      preset[i].weight = ((preset[i].weight / sum_total) * 100);

      if (preset[i].weight > largest_weight) {
        largest_weight = preset[i].weight;
        largest_i = i;
      }

      new_total += preset[i].weight;
    }

    if (new_total != 100) {
      var difference = (100 - new_total);
      preset[largest_i].weight += difference;
    }
  }

  function expandSlice(i, amount) {

    var change = Math.min(99 - preset[i].weight, amount);

    if (change > 0) {
      preset[i].weight += change;

      // Shrink other slices by a total of the same amount
      var c = change / (preset.length - 1);

      for (var j = 0; j < preset.length; j++) {
        if (i != j) {
          preset[j].weight = Math.max(100 - minimum_slice_percentage, preset[j].weight - c);
        }
      }

      normalizeData();
      setTooltips();
      animateSlices(250);
      updateFields();
    }
  }

  function shrinkSlice(i, amount) {

    var change = Math.min(preset[i].weight - (100 - minimum_slice_percentage), amount);

    if (change > 0) {
      preset[i].weight -= change;

      // expand other slices by a total of the same amount
      var c = change / (preset.length - 1);

      for (var j = 0; j < preset.length; j++) {
        if (i != j) {
          preset[j].weight = Math.min(minimum_slice_percentage, preset[j].weight + c);
        }
      }

      normalizeData();
      setTooltips();
      animateSlices(250);
      updateFields();
    }
  }
  
  function addSlice(i) {
    var val = 360 / (100 * preset[i].weight);
    var slice = r.path().attr({
      slice: [i, dish_x, dish_y, 1, 0, val],
      stroke: "#000",
      "stroke-width": stroke_width_slice
    });

    slice.i = i;
    slice.click(function () {
      expandSlice(i, 5);
    });
    slice.mouseover(function () {
      document.body.style.cursor = 'pointer';
      slice_over = i;
    });

    slice.mouseout(function () {
      document.body.style.cursor = 'default';
      slice_over = -1;
    });

    slices.push(slice);
  }

  // draw key right of pie chart
  var kbox_size = 10;
  var key_top = 0;

  function addKey(i) {

    var label = preset[i].label;
    var colour = preset[i].colour;

    var key = r.set();
    space_from_graph = 10;
    space_from_colour_swatch = 15;

    key.push(r.rect((dish_x * 2) + space_from_graph, key_top + i * kbox_size * 2, kbox_size, kbox_size, 0)
      .attr({
        fill: colour,
        stroke: "#000",
        "stroke-width": stroke_width_swatch_border
      }));
    key.push(r.text((dish_x * 2) + space_from_graph + space_from_colour_swatch, key_top + 5 + i * kbox_size * 2, 'label').attr({
      text: label,
      font: '100 14px "Helvetica Neue", Helvetica, "Arial Unicode MS", Arial, sans-serif',
      fill: "#000",
      "text-anchor": "start"
    }));
    key.attr({
      opacity: 0
    });

    keys.push(key);
  }

  function fadeIn(set, ms) {
    set.animate({
      opacity: 1
    }, ms || 1000);
  }

  function removeAll(set) {
    set.forEach(function (e) {
      e.remove();
    });
    set.clear();
  }

  function pieChartIntro() {

    removeAll(slices);
    removeAll(keys);

    // Walk over our targets building the preset
    for (field in targets) {
      this_preset = new Array();
      field_id = '#' + targets[field];
      wrapper_id = field_id + '-wrapper';

      // The field Label
      label = $(wrapper_id + ' label').
      text(). // Get the text of the label
      replace(/[*:]/g, ''). // tidy up
      trim();

      // The value we currently have
      val = $(field_id).val();

      // Quick hack to get a consistant hex val for any given label
      colour_hash = CryptoJS.MD5(label).toString();
      colour = '#' + colour_hash.substring(0, 6);

      // Build this preset
      this_preset['label'] = label;
      this_preset['weight'] = parseFloat(val);
      this_preset['colour'] = colour;
      this_preset['target_id'] = field_id;

      // Add it to our preset array
      preset.push(this_preset);

      // Hide our field
      $(wrapper_id).addClass('element-invisible');
    }

    key_top = (r.height - kbox_size * 2 * preset.length) * 0.5;

    pie_dish = r.circle(dish_x, dish_y, 0).attr({
      "stroke": "#000",
      "stroke-width": stroke_width_pie_dish,
      "fill": "#aaa"
    });
    pie_dish.animate({
      "r": dish_r
    }, 1000, 'linear');
    normalizeData();
    for (i = 0; i < preset.length; i++) {
      addSlice(i, dish_x, dish_y);
      addKey(i);
    }
    setTooltips();
    fadeIn(keys);
    animateSlices(1000, dish_x, dish_y, dish_r);
  }

  /**
   * Update the Number fields with the current values.
   * TODO: This could be set up to populate the fields with values that are a
   *       ratio of the original total of all the number fields.
   */
  function updateFields() {
    for (i = 0; i < preset.length; i++) {
      $(preset[i]['target_id']).val(preset[i]['weight']);
    }
  }
  
  pieChartIntro();
}
